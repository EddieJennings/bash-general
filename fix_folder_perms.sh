#!/bin/bash
# Sets folder permissions for all directories (including sub directories to 755)

echo "####################################################"
echo "# Welcome to the fix directory permissions script! #"
echo "####################################################"
echo ""
echo "######"
echo "# NOTE:  This script will set the permissions to 755 on"
echo "#        all directories and sub-directories within the path provided"
echo "######"
echo ""

# Get the path
PATH_UNVERIFIED=true
while [ "$PATH_UNVERIFIED" == "true" ] ; do
        echo "$PATH_UNVERIFIED"
        echo "Enter the desired path"
        read PATH_TO_FIX
        export PATH_TO_FIX

        echo "Is $PATH_TO_FIX the desired path? [y/n]"
        read PATH_CHECK
        case $PATH_CHECK in
                [yY])
                        PATH_UNVERIFIED=false
                        ;;
                *)
                        PATH_UNVERIFIED=true
                        ;;
        esac
done
   
# Set the permissions     
echo "Fixing directories in path: $PATH_TO_FIX"

DIRS_TO_CHECK=$(find $PATH_TO_FIX -type d -not -perm 755)

for i in $DIRS_TO_CHECK ; do
        echo "Fixing $i"
        chmod 755 $i
done


