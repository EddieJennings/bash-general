#!/bin/bash
#  Little game for practicing array

echo "Here's a little game for working with arrays."
echo ""
echo ""
echo "######################"
echo "# Favorite Word Game #"
echo "######################"
echo ""
echo "Enter your favorite word (other than QUIT).  Enter QUIT (all caps) to exit."
read FAVWORD
echo ""

COUNT=0

# Check first entry

if [ $FAVWORD != "QUIT" ]
then
	FAVWORD_ARRAY=($FAVWORD)
	echo "Favorite word array element $COUNT is ${FAVWORD_ARRAY[$COUNT]}."
	echo ""
	echo "Enter another favorite word (other than QUIT).  Enter QUIT (all caps) to exit."
	read FAVWORD
	echo ""
else
	echo "You did not enter any favorite words."
	echo ""
	echo "Thanks for playing!"
	exit 0
fi

# Check subsequent entries

until [ $FAVWORD == "QUIT" ]
do
	((COUNT++))
	FAVWORD_ARRAY[$COUNT]=$FAVWORD
	echo "Favorite word array element $COUNT is ${FAVWORD_ARRAY[$COUNT]}."
	echo ""
	echo "Enter another favorite word (other than QUIT).  Enter QUIT (all caps) to exit."
	read FAVWORD
	echo ""
done

# Display the results

echo "Here are all of the favorite words you entered:"
echo ${FAVWORD_ARRAY[@]}
echo ""

echo "Thanks for playing!"
	
	
