#!/bin/bash

# Script for creating local user accounts from a CSV file

echo "This script will create provided CSV file."
echo ""
echo "###################################################################"
echo "# Note:  The format of the CSV data should be 'username,password' #"
echo "###################################################################"
echo ""

SOURCEFILE="/tmp/testusers.csv" # Set this to your desired CSV

# Set internal field separator to newline
OLDIFS=$IFS
IFS=$'\n'

for i in $(cat $SOURCEFILE)
do
	NEW_USER=$(echo $i | cut -d ',' -f 1)
	NEW_PASSWORD=$(echo $i | cut -d ',' -f 2)
	
	echo "Create user $NEW_USER with password $NEW_PASSWORD"
	
	useradd $NEW_USER
	echo $NEW_PASSWORD | passwd $NEW_USER --stdin
done

# Reset IFS
IFS=$OLDIFS

echo "All done!"

# To-do list
# 1.  Have script check to see if it's being run as root
# 2.  Allow user to input the file when running the script (script.sh my_file.csv)
# 3.  Validate CSV format
