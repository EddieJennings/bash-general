#!/bin/bash

echo "Welcome to the parameter echoing script!"
echo ""

if [ "$#" -gt 0 ]; then
    declare -i COUNT=1
    
    for i in $@; do
        echo "The value for argument \$$COUNT is:"
        echo $i
        ((COUNT++))
    done
    
    else
        echo "There were no parameters given."
        echo "Try using ./loop_through_args.sh foo blah"
fi

echo ""
echo "All done!"
