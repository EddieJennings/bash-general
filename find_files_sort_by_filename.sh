#!/bin/bash

# For Dustin at MangoLassi
# Search for files recursively and sort the list by file name rather than by file path, regardless of extension.
echo "WARNING:  This script IS NOT written to handle file names with spaces."
read -p $'Press ENTER to continue or CTRL+C to quit.\n'

echo "Enter the full path of the root directory to search"
read ROOT_DIRECTORY

echo "Determining temporary file name."

TEMP_FILE="/tmp/filesearchtemp$(date +%F%H%M%S)"
while [ -f "$TEMP_FILE" ]; do
    sleep 1
    TEMP_FILE="/tmp/filesearchtemp$(date +%F%H%M%S)"
done

echo "Determining output file name."

if [ "$(id -u)" != 0 ];
    then
        OUT_FILE="/home/$(whoami)/file_search_results.txt"
        while [ -f "$OUT_FILE" ]; do
            sleep 1
            OUT_FILE="/home/$(whoami)/file_search_results$(date +%F%H%M%S).txt"
        done
    else
        OUT_FILE="/root/file_search_results.txt"
        while [ -f "$OUT_FILE" ]; do
            sleep 1
            OUT_FILE="/root/file_search_results$(date +%F%H%M%S).txt"
        done
fi

echo "Recursively gathering all files in $ROOT_DIRECTORY."

ALL_FILES=$(find $ROOT_DIRECTORY -type f)

echo "Determining unique file names."

for i in $ALL_FILES; do
    INDFILE=$(echo $i | rev | cut -d '/' -f 1 | cut -d '.' -f 2- | rev)
    echo $INDFILE >> $TEMP_FILE
done

UNIQUE_FILE_NAMES=$(cat $TEMP_FILE | sort -u)

echo "Verifying files found and creating desired format."

for j in $UNIQUE_FILE_NAMES; do
    find $ROOT_DIRECTORY -type f -name "$j.*" | sort | tee -a $OUT_FILE
done

echo "Output has been saved to $OUT_FILE."
echo "Cleaning up temp file, $TEMP_FILE."

rm $TEMP_FILE

echo "Finished!"
