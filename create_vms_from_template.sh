#!/bin/bash
# Clone VMs from a template and run virt-sysprep

VM=("lab-ansible-web01" "lab-ansible-web02" "lab-ansible-db01" "lab-ansible-db02" "lab-ansible-host01")

for i in ${VM[@]} ; do
        virt-clone --original rhelpractice1minimal --name $i \
        --file "/kvm/vm/$i-1.qcow2" --file "/kvm/vm/$i-2.qcow2" #two files because original has two disks
        
        # Need to make this a bit more efficient
        # Need to look about having credentials stored in some kind of file
        virt-sysprep -d $i --hostname $i --password eddie:password:MYPASSWORD
done
